/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.
*/

    function register(user)
    {
        if(user === null || user === "" || user === undefined)
        {
            alert("Registration failed. Please enter a name.");
        }
        else if(registeredUsers.includes(user))
        {
            alert("Registration failed. Username already exists!");
            console.log(registeredUsers);
        }
        else
        {
            registeredUsers.push(user);
            alert("Thank you for registering!");
            console.log(registeredUsers);
        }
    }

    register();

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
*/

    function addFriend(user)
    {
        let foundUser1 = registeredUsers.includes(user);
        let foundUser2 = friendsList.includes(user);

        if(foundUser2)
        {
            alert("You are already friends with " + user + ".");
        }
        else if(foundUser1)
        {
            friendsList.push(user);
            alert("You have added " + user + " as a friend!");
            console.log(friendsList);
        }
        else
        {
            alert("User not found. Please try again!");
        }
    }

    addFriend();

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
*/
    
    function displayFriends()
    {
        if(friendsList.length === 0)
        {
            alert("You currently have 0 friends. Add one first.");
        }
        else
        {
            friendsList.forEach(function()
            {
            console.log(friendsList.join("\n"));
            })
        }
    }

    displayFriends();

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function
*/

    function displayNumberofFriends()
    {
        if(friendsList.length === 0)
        {
            alert("You currently have 0 friends. Add one first.");
        }
        else if(friendsList.length === 1)
        {
            alert("You only have 1 friend in your friends list.");
            console.log(friendsList);
        }
        else
        {
            alert("You currently have " + friendsList.length + " friends.");
            console.log(friendsList);
        }
    }

    displayNumberofFriends();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.
*/

    function deleteFriend()
    {

        if(friendsList.length === 0)
        {
            alert("You currently have 0 friends. Add one first.");
        }
        else
        {
            let deletedFriend = friendsList.pop();
            alert("You have deleted " + deletedFriend + " in your friends list.");
            console.log(friendsList);
        }
    }

    deleteFriend();

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().
*/

    function deleteSpecificFriend(user)
    {
        let index = friendsList.indexOf(user);
        let deleteCount = 1;
        let foundUser = friendsList.includes(user);

        if(user === null || user === "" || user === undefined)
        {
            alert("You need to specify a user to delete!");
        }
        else if(foundUser)
        {
            friendsList.splice(index, deleteCount);
            alert("You have deleted " + user + " from your friends list");
            console.log(friendsList);
        }
        else
        {
            alert("User does not exist. Please try again!");
            console.log(friendsList);
        }
    }

    deleteSpecificFriend();
